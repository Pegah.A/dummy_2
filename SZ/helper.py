import os
import pandas as pd
import numpy as np
from shutil import copyfile

def get_info_of_a_donor(donor_id):
    schiz_info_csv = "/Users/pegah_abed/Documents/old_Human_ISH/after_segmentation/dummy_2/SZ/human_ISH_info.csv"
    schiz_info_df = pd.read_csv(schiz_info_csv)

    schiz_info_df = schiz_info_df[schiz_info_df['donor_id']==donor_id]

    print (schiz_info_df)

    schiz_images = list(schiz_info_df["image_id"])
    print("Number of images: ", len(schiz_images))
    schiz_uq_images = list(set(schiz_images))
    print("Number of unique images: ", len(schiz_uq_images))

    print (schiz_uq_images)

    genes = list(schiz_info_df['gene_symbol'])
    print("Number of genes: ", len(genes))
    schiz_uq_genes = list(set(genes))
    print("Number of unique genes: ", len(schiz_uq_genes))
    # print (sorted(schiz_uq_genes))

    donors = list(schiz_info_df['donor_id'])
    print("Number of donors: ", len(donors))
    uq_donors = list(set(donors))
    print("Number of unique donors: ", len(uq_donors))
    print(uq_donors)




def copy_images_of_donor():

    image_list = [81239552, 81240067, 81239557, 81240072, 81239562, 81240077, 81239567, 81240082, 81239572, 81240087,
                  81239577, 81240092, 81239582, 81240097, 81239587, 81240102, 81239592, 81240107, 81239597, 81240112,
                  81239602, 81240117, 81239607, 81239612, 81239617, 81239627, 81239632, 81239637, 81239642, 81239132,
                  81239647, 81239137, 81239652, 81239142, 81239657, 81239147, 81239662, 81239152, 81239667, 81239157,
                  81239672, 81239162, 81239677, 81239167, 81239682, 81239172, 81239687, 81239692, 81239182, 81239697,
                  81239187, 81239702, 81239192, 81239707, 81239197, 81239712, 81239202, 81239717, 81239207, 81239212,
                  81239727, 81239217, 81239732, 81239737, 81239227, 81239742, 81239747, 81239237, 81239752, 81239242,
                  81239757, 81239247, 81239252, 81239767, 81239257, 81239772, 81239262, 81239777, 81239267, 81239782,
                  81239272, 81239787, 81239277, 81239282, 81239287, 81239292, 81239807, 81239297, 81239812, 81239302,
                  81239817, 81239307, 81239312, 81239827, 81239317, 81239832, 81239837, 81239327, 81239842, 81239332,
                  81239847, 81239337, 81239852, 81239342, 81239857, 81239347, 81239862, 81239352, 81239867, 81239357,
                  81239872, 81239362, 81239877, 81239367, 81239882, 81239372, 81239887, 81239377, 81239892, 81239382,
                  81239897, 81239387, 81239902, 81239392, 81239907, 81239397, 81239912, 81239402, 81239917, 81239407,
                  81239412, 81239927, 81239417, 81239932, 81239937, 81239427, 81239942, 81239432, 81239947, 81239437,
                  81239952, 81239442, 81239447, 81239962, 81239452, 81239967, 81239972, 81239977, 81239982, 81239987,
                  81239477, 81239992, 81239482, 81239997, 81240002, 81239492, 81240007, 81239497, 81240012, 81239502,
                  81240017, 81239507, 81239512, 81240027, 81239517, 81240032, 81240037, 81240042, 81239532, 81240047,
                  81239537, 81240052, 81239542, 81240057, 81239547, 81240062]



    donor_folder_name = "H08-0097"
    original_path = "/external/rprshnas01/netdata_kcni/lflab/SiameseAllenData/human_ISH/human_ish_data/schizophrenia/images"

    copy_to_path = os.path.join("/external/rprshnas01/netdata_kcni/lflab/SiameseAllenData/human_ISH/dummy/dummy_2/SZ", donor_folder_name)
    if not os.path.exists(copy_to_path):
        os.mkdir(copy_to_path)


    counter = 0
    for item in image_list:
        img = str(item) + ".jpg"

        img_to_copy = os.path.join(original_path, img)
        copyfile(img_to_copy, os.path.join(copy_to_path, img))
        counter +=1



    print ("finished copying {} images".format(counter))




if __name__ == "__main__":

    copy_images_of_donor()